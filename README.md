# Mautic Kubernetes Operator

This is a Kubernetes Operator for [Mautic](https://www.mautic.org/). Currently it's not ready for production use. 
This operator will create the following objects:

 - The deployment for the application.
 - The MySQL password secret.
 - The service.
 - The ingress.  

## Getting started

Deploy the CRD:

    $ kubectl create -f deploy/crds/mautic_v1alpha1_mautic_crd.yaml

Deploy the mautic-operator:

    $ kubectl create -f deploy/service_account.yaml
    $ kubectl create -f deploy/role.yaml
    $ kubectl create -f deploy/role_binding.yaml
    $ kubectl create -f deploy/operator.yaml

Verify that the memcached-operator is up and running:

    $ kubectl get deployment
    NAME                     DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
    mautic-operator       1         1         1            1           1m


Deploy a Mautic resource:

    apiVersion: mautic.bujupi.me/v1alpha1
    kind: Mautic
    metadata:
      name: example-mautic
      namespace: default
    spec:
      size: 1
      image: mautic/mautic:fpm
      dbHost: mysql:3306
      dbName: mautic
      dbUser: root
      dbPassword: password
      ingressName: mautic.bujupi.me

