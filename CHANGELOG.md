# v0.2
----------
* Fixed the role for creating ingresses.
* The operator is now more portable. You can define in the specs of Mautic if you want to deploy the service and the ingress. By default they are enabled. 


# v0.1 
----------
This is the initial release.